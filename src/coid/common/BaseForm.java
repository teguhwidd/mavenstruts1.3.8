package coid.common;

import org.apache.struts.action.ActionForm;

public class BaseForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6955909823415551350L;
	private String task;
	


	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
}
