package coid.coba.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import coid.coba.form.HelloForm;

public class HelloAction extends Action {
	//exec 
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HelloForm helloForm = (HelloForm) form;
		String pageForward = "showPage";
		
		if(helloForm.getTask() != null && "teguh".equals(helloForm.getName())) {
			pageForward = "showWelcome";
		}else if(helloForm.getName() != null && helloForm.getName() != "teguh") {
			ActionErrors errs = new ActionErrors();
			errs.add("", new ActionMessage("hello.msg.err"));
			saveErrors(request, errs);
		}
		return mapping.findForward(pageForward);
	}
}
