package coid.coba.action;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.google.gson.Gson;

import coid.coba.form.AjaxForm;

public class AjaxAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		AjaxForm frm = (AjaxForm) form;
//		HttpSession session = request.getSession();
		String task = frm.getTask();
		String pageForward = "showResult";
//	    gLog.info("=========================="+task);

		request.setAttribute("className", "WebCompHandler");
		request.setAttribute("task", task);
		String targetObjectName = request.getParameter("targetObjectName");
		String parameterIn1 = request.getParameter("parameterIn1");

		System.out.println("=============== task = " + task);
		if ("getInfoPerusahaanKerjasama".equals(task)) {
			frm.setData1(parameterIn1);
			frm.setData2(parameterIn1);
			HashMap<String, String> mapOutput = new HashMap<String, String>();
			mapOutput.put("code", frm.getData1());
			mapOutput.put("desc", frm.getData2());

			Gson gson = new Gson();
			String json = gson.toJson(mapOutput);

			System.out.println("JSON Parameter : \n" + json);

			request.setAttribute(targetObjectName, json);
			request.setAttribute("targetObjectName", targetObjectName);
		} else if ("getListofTipeAgunan".equals(task)) {

			frm.setData1(parameterIn1);
			frm.setData2(parameterIn1);
			List<String> listData = frm.getListdata();

			Gson gson = new Gson();
			String json = gson.toJson(listData);

			System.out.println("JSON Parameter : \n" + json);
			request.setAttribute(targetObjectName, json);
			request.setAttribute("targetObjectName", targetObjectName);

		}

		return mapping.findForward(pageForward);
	}

}
