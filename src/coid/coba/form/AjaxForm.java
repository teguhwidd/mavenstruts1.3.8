package coid.coba.form;

import java.util.List;

public class AjaxForm extends BaseForm {
	private static final long serialVersionUID = -65449963406164557L;
	private String data1;
	private String data2;
	private List<String> Listdata;

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public List<String> getListdata() {
		return Listdata;
	}

	public void setListdata(List<String> listdata) {
		Listdata = listdata;
	}
}
