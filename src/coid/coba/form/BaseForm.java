package coid.coba.form;

import org.apache.struts.action.ActionForm;

public class BaseForm extends ActionForm{
	private static final long serialVersionUID = -2336637794612900413L;
	private String task;
	
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
}