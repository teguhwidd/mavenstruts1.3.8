package coid.coba.form;

public class HelloForm extends BaseForm {
	private static final long serialVersionUID = 2562333875165973559L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
