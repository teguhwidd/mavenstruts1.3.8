<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hello Page Input</title>
</head>
<body>
	<html:form action="hello">
		<html:text property="name" value=""/>
		<html:hidden property="task" value="showPage"/>
		<html:submit value="Masuk" />
		<hr />
		<html:errors/>
		
	</html:form>
</body>
</html>