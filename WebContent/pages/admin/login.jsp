<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/pages/admin/decorator/pageheader.jsp" />
    <style>
    </style>
</head>
<body style="nopadding nomargin" class="bg">
    <?php //$this->load->view('admin/include/navbar'); ?>
    <div class="container">
        <div class="form" style="">
            <div class="thumbnail"><img src="<?php echo base_url(); ?>assets/img/bardosono.png" alt="Silahkan Login"/></div>
            <?php echo $this->session->flashdata('pesan'); ?>
            <form action="<?php echo site_url('xpage/login'); ?>" class="login-form" name="myForm" id="myForm" method="POST">
                <input type="text" placeholder="Masukan Username" id="username" name="username" value="<?php echo $this->session->userdata('username'); ?>" autocomplete="off"/>
                <span for="username" class="error"><?php echo form_error('username'); ?></span>
                <input type="password" placeholder="Masukan Password" id="password" name="password" value="<?php echo $this->session->userdata('password'); ?>"/>
                <span for="password" class="error"><?php echo form_error('password'); ?></span><br>
                <div class="captcha" style="">
                    <center style=""><img src="<?php echo $captcha['image_src'];?>" alt="CAPTCHA security code" /></center>
                    <input type="text" placeholder="Masukan CAPTCHA" id="captcha" name="captcha" value="" autocomplete="off"/>
                    <span for="captcha" class="error"><?php echo form_error('captcha'); ?></span>
                </div>
                <button id="loginBtn">Login</button>
                <!-- <center><p class="message">Belum terdaftar ? <a href="<?php echo site_url('page/daftar'); ?>">Buat Akun.</a> -->
                    <!-- <br> <a href="<?php echo site_url('page/daftar'); ?>">Lupa password ?</a></p> -->
                <!-- </center> -->
            </form>
        </div>
    </div>
    <!-- /.container -->
    <?php //$this->load->view('admin/include/footer'); ?>

    <!-- JavaScript -->
    <?php $this->load->view('admin/include/pagefooter'); ?>
    <!-- JavaScript -->
    <script>
    $(function() {
        var i1 = "Username"; var i2= "Password"; i3= "CAPTCHA";
        $("form[name='myForm']").validate({
            rules: {
                username: {required: true, },
                password: {required: true, maxlength: 16},
                captcha: {required: true},
            },
            messages: {
                username: {required: i1+" harus diisi !!!"},
                password: {required: i2+" harus diisi !!!", maxlength: i2+" maksimal 16 karakter !!!"},
                captcha: {required: i3+" harus diisi !!!"},
            }, submitHandler: function(form) { form.submit(); }
        });
    });

    $(document).ready(function(){
        $(".alertme").fadeIn("fast").show().delay(3000).fadeOut("slow");
        $("#password" ).focusout(function() { $("#captcha").focus(); });
        $("input" ).click(function(){ $(this).select(); });
        $("input").on("change paste keyup", function() {$("span.error").html(""); });
    });
    $("<?php echo $this->session->flashdata('id'); ?>").focus().select();
    </script>
</body>
</html>
